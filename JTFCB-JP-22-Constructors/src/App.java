class Machine {
	private String name;
	private int code;

	// constructor bez parametru
	public Machine() {
		// musi byc w pierwszej linii
		// przez to odpala sie drugi znowu
		// przywoluje ten co bierze dwa parametry
		this("Arnie", 0);
		System.out.println("Constructor running!");

		// prostszy zapis
		// name = "Arnie";
	}

	// constructor z parametrem

	public Machine(String name) {

		System.out.println("Second constructor running");
		this.name = name;
	}

	public Machine(String name, int code) {

		System.out.println("Third constructor running");
		this.name = name;
		this.code = code;
	}

}

public class App {

	public static void main(String[] args) {
		Machine machine1 = new Machine();
		
		// szuka konstruktora z wprowadzeniem stringa
		Machine machine2 = new Machine("Bertie");
		
		// szuka konstruktora z dwoma parametrami
		Machine machine3 = new Machine("Chalky", 7);
		// zapis do konstruktora
		// new Machine();
	}

}
